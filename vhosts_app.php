#!/usr/bin/php
<?php

class ApacheVhostsGenerator
{

    protected $sites = array();
    protected $config = array();
    protected $configFile;

    public function __construct() {

    }

    protected function cfg($param) {
        if (!isset($this->config[$param])) Throw new Exception('config param not found');
        return $this->config[$param];
    }

    protected function findDocumentRoot($sitename) {
        $publicNames = $this->cfg('public_names');
        $sitesDirs = $this->cfg('sites_dirs');

        $pubdir = false;
        $rootdir = $sitesDirs . '/' . $sitename;
        foreach ($publicNames as $val) {
            if (is_dir($rootdir . '/' . $val)) {
                $pubdir = $rootdir . '/' . $val;
                break;
            }
        }

        return ($pubdir) ? $pubdir : $rootdir;
    }


    protected function renderSiteConfig($sitename) {
        $document_root = $this->findDocumentRoot($sitename);
        $sitesLogs = $this->cfg('sites_logs');
        $mpm_user = $this->cfg('mpm_user');
        $mpm_group = $this->cfg('mpm_group');

        $includeArea = $this->cfg('sites_dirs') . '/' . $sitename . '.conf';
        $includeArea = (is_file($includeArea)) ? file_get_contents($includeArea) : '';

        $content = <<<EOT
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    ServerName $sitename
    ServerAlias www.$sitename
    DocumentRoot $document_root
    $includeArea
    <Directory $document_root>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>

	<IfModule mpm_itk_module>
		AssignUserId $mpm_user $mpm_group
	</IfModule>

    ErrorLog $sitesLogs/$sitename-error.log
    LogLevel warn
    CustomLog $sitesLogs/$sitename-access.log combined
</VirtualHost>


EOT;

        return $content;
    }


    protected function createApacheConfig() {
        $content = '';

        foreach ($this->sites as $sitename) {
            $content .= $this->renderSiteConfig($sitename);
        }

        $apachefile = $this->cfg('apache_file');
        file_put_contents($apachefile, $content);
    }


    protected function createHostsFile() {
        $hostsdata = file($this->cfg('hosts_file'));

        foreach ($hostsdata as $key => $val) {
            $val = trim($val);

            $hostsdata[$key] = $val;

            foreach ($this->sites as $site){
                if (false !== strpos($val, $site)){
                    unset($hostsdata[$key]);
                }
            }
        }

        foreach ($this->sites as $sitename) {
            $hostsdata[] = "127.0.0.1\t$sitename";
        }

        $hostsdata = array_values($hostsdata);

        $content = implode("\n", $hostsdata);

        $hostsfile = $this->cfg('hosts_file');
        file_put_contents($hostsfile, $content);
    }

    protected function loadConfig() {
        $this->configFile = dirname(__FILE__) . '/config.php';

        if (!file_exists($this->configFile)) {
            Throw new Exception('config file not found');
        }

        $config = include($this->configFile);


        $this->config = $config;
    }


    protected function createConfig() {
        $data = array(
            'apache_file' => '/etc/apache2/sites-enabled/vhosts_app.conf',
            'apache_restart' => '/etc/init.d/apache2 restart',
            'hosts_file' => '/etc/hosts',
            'sites_dirs' => '/home/user/webserv',
            'sites_logs' => '/home/user/webserv/logs',
            'sites_mask' => 'localhost',
            'public_names' => array('htdocs', 'public_html', 'www', 'web'),
            'mpm_user' => 'user',
            'mpm_group' => 'group',
            'exclude_dirs' => array()
        );

        $data = "<?php return " . var_export($data, 1) . ";";
        file_put_contents($this->configFile, $data, LOCK_EX);
    }


    protected function findSites() {
        $exclude_dirs = $this->cfg('exclude_dirs');

        $handle = opendir($this->cfg('sites_dirs'));

        $regexp = '@.*\.' . $this->cfg('sites_mask') . '@';

        while (false !== ($file = readdir($handle))) {
            if ($file != '.' && $file != '..' && is_dir($this->cfg('sites_dirs') . '/' . $file) && preg_match($regexp, $file)) {

                if (!in_array($file, $exclude_dirs)){
                    $this->sites[] = $file;
                }


            }
        }

        closedir($handle);
    }


    public function generate() {
        try {
            $this->loadConfig();
            $this->findSites();

            $bkpDir = dirname(__FILE__) . '/bkp';
            if (!is_dir($bkpDir)){
                mkdir($bkpDir);
            }
            
            $ok2 = copy($this->cfg('hosts_file'), $bkpDir . '/hosts' . date('d-m-Y_H-i-s'));

            if (file_exists($this->cfg('apache_file'))) {
                $ok1 = copy($this->cfg('apache_file'), $bkpDir . '/apache' . date('d-m-Y_H-i-s'));
            } else {
                $ok1 = true;
            }

            if ($ok1 && $ok2) {


                $this->createApacheConfig();
                $this->createHostsFile();

                system($this->cfg('apache_restart'));
            }

        } catch (Exception $e) {
            $this->createConfig();
        }

    }

}


$app = new ApacheVhostsGenerator;
$app->generate();

