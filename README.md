# Создаем виртуальные хосты для apache2 (linux) исходя из структуры каталогов #

webserver direcory: /home/user/webserv/web

site1

```
#!

/home/user/webserv/web/site1.localhost
/home/user/webserv/web/site1.localhost/htdocs
```


site2

```
#!

/home/user/webserv/web/site2.localhost
/home/user/webserv/web/site1.localhost/www
```


первый запуск скрипта **php vhosts_app.php**
создаст конфиг, если его нет, конфиг надо отредактировать проставив
пути до директории с сайтами

второй запуск **sudo php vhosts_app.php**
просканирует содержимое директории webserv/web

создаст виртуальные хосты, добавит записи в hosts и рестартанет апач

пример конфига
```
#!php
	$data = array(
	  'apache_file' => '/etc/apache2/sites-enabled/vhosts_app', //файл с виртуальными хостами
	  'apache_restart' => '/etc/init.d/apache2 restart', //команда для рестарта апача
	  'hosts_file' => '/etc/hosts', // путь до hosts
	  'sites_dirs' => '/home/user/webserv', //коневая директория для сканирования
	  'sites_logs' => '/home/user/webserv/logs', //директория для логов
	  'sites_mask' => 'local', //маска для директорий-сайтов
	  'public_names' => array('htdocs', 'public_html', 'www', 'web'), //публичные директории сайтов 
	  'mpm_user' => 'user',  //пользователь от которого будет работать апач (требуется mpm-itk)
	  'mpm_group' => 'group' //группа 
	  'exclude_dirs' => array('test123.local')
	);

```